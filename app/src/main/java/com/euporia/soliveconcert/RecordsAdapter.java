package com.euporia.soliveconcert;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;



import com.euporia.soliveconcert.models.Current.Fields;
import com.euporia.soliveconcert.models.opendata.Records;
import com.euporia.soliveconcert.ui.Constant;
import com.squareup.picasso.Picasso;

import java.util.List;


public class RecordsAdapter extends ArrayAdapter<Records> {

    private int resId;

    public RecordsAdapter(Context context, int resource, List<Records> objects) {
        super(context, resource, objects);

        resId = resource; // R.layout.item_Records
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // chargement du layout
        convertView = LayoutInflater.from(getContext()).inflate(resId, null);

        // récupération des Views
        TextView textViewTitle = convertView.findViewById(R.id.textViewTitle);
        TextView textViewDatesStart = convertView.findViewById(R.id.textViewDateStart);
        TextView textViewPlacename = convertView.findViewById(R.id.textViewPlacename);
        TextView textViewPricingInfo = convertView.findViewById(R.id.textViewPricing_info);
        TextView textViewAddress = convertView.findViewById(R.id.textViewAddress);
        TextView textViewCity = convertView.findViewById(R.id.textViewCity);
        TextView textViewCityDistric = convertView.findViewById(R.id.textViewCityDistric);
        TextView textViewSpaceTimeInfo = convertView.findViewById(R.id.textViewSpaceTimeInfo);
        ImageView imageViewfiedls = convertView.findViewById(R.id.imageViewfiedls);


        // récupération de l'objet Records
        Records item = getItem(position);
        if (item != null) {
            // affichage des données de spectacles
            textViewTitle.setText(item.getFields().getTitle());
            textViewDatesStart.setText(item.getFields().getDate_start());
            textViewPlacename.setText(item.getFields().getPlacename());
            textViewPricingInfo.setText(item.getFields().getPricing_info());
            textViewAddress.setText(item.getFields().getAddress());
            textViewCity.setText(item.getFields().getCity());
            textViewCityDistric.setText(item.getFields().getCity_district());
            textViewSpaceTimeInfo.setText(item.getFields().getSpace_time_info());
            // affichage de l'image
            String urlImage = String.format(Constant.PARAM_RECORDS, item.getFields().getImage_thumb());
            Picasso.get().load(urlImage).into(imageViewfiedls);

        }

        return convertView;

    }
}
