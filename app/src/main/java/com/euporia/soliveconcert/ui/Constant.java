package com.euporia.soliveconcert.ui;

import java.util.Locale;

public class Constant {
    public static final String URL_OPENDATA = "https://opendata.paris.fr/api/records/1.0/search/" +
            "?dataset=evenements-a-paris&facet=tags&facet=placename&facet=department&facet=region&" +
            "facet=city&facet=date_start&facet=date_end&facet=pricing_info&facet=updated_at";

    public static final String PARAM_OPENGEO = "geometry";
    public static final String PARAM_CURRENT = "fields";
    public static final String PARAM_RECORDS = "records";

}

