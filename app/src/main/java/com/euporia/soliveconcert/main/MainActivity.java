package com.euporia.soliveconcert.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.euporia.soliveconcert.R;
import com.euporia.soliveconcert.controller.HomeActivity;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private Timer myTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myTimer = new Timer(); // instance de classe

        // déclaration d'un objet de type TimerTask
        TimerTask monTimerTask = new TimerTask() {
            @Override
            public void run() {
                Log.e("Main", "Lancer HomeActivity");

                Intent myIntent = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(myIntent);
                finish(); // ou android:noHistory="true" dans le Manifest
            }
        };

        myTimer.schedule(monTimerTask, 2000); // 2000 = durée en milliseconds = 2 secondes
    }
}

