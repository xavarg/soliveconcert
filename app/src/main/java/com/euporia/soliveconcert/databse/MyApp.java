package com.euporia.soliveconcert.databse;


import android.app.Application;

import com.raizlabs.android.dbflow.config.FlowManager;

public class MyApp extends Application {

    public void OnCreate(){
        super.onCreate();
        FlowManager.init(this);
    }
}
