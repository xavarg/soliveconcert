package com.euporia.soliveconcert.databse;

import com.raizlabs.android.dbflow.annotation.Database;
import com.raizlabs.android.dbflow.annotation.Migration;
import com.raizlabs.android.dbflow.sql.SQLiteType;
import com.raizlabs.android.dbflow.sql.migration.AlterTableMigration;

@Database(name = AppDatabase.NAME, version = AppDatabase.VERSION)
public class AppDatabase  {

    public static final String NAME="AppDatabase";
    public static final  int VERSION = 2;

    @Migration(version = 2, database = AppDatabase.class)
    public static class Migration2 extends AlterTableMigration<User>{

        public Migration2(Class<User> table) {
            super(table);
        }

        public void onPreMigrate(){
            super.onPreMigrate();
            addColumn(SQLiteType.TEXT, "pseudo");
        }
    }
}
