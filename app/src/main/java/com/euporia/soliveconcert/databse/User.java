package com.euporia.soliveconcert.databse;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

import retrofit2.http.PUT;

@Table(database = AppDatabase.class)
public class User extends BaseModel implements Serializable {

    @PrimaryKey(autoincrement = true)
    public int id;

    @Column
    public String pseudo;

    @Column
    public String firstName;

    @Column
    public String LastName;

    @Column
    public String email;

    @Column
    public String passWord;

    public User() {

    }

    public User(int id, String pseudo, String firstName, String lastName, String email, String passWord) {
        this.id = id;
        this.pseudo = pseudo;
        this.firstName = firstName;
        LastName = lastName;
        this.email = email;
        this.passWord = passWord;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPseudo() {
        return pseudo;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

}
