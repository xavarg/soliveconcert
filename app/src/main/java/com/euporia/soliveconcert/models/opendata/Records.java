package com.euporia.soliveconcert.models.opendata;

import com.euporia.soliveconcert.models.Current.Fields;
import com.euporia.soliveconcert.models.Current.Geometry;

public class Records {

    private Fields fields;
    private String datasetid;
    private String recordid;
    private String records_timestap;
    private Geometry geometry;

    public String getDatasetid() {
        return datasetid;
    }

    public void setDatasetid(String datasetid) {
        this.datasetid = datasetid;
    }

    public String getRecordid() {
        return recordid;
    }

    public void setRecordid(String recordid) {
        this.recordid = recordid;
    }

    public String getRecords_timestap() {
        return records_timestap;
    }

    public void setRecords_timestap(String records_timestap) {
        this.records_timestap = records_timestap;
    }

    public Fields getFields() {
        return fields;
    }

    public void setFields(Fields fields) {
        this.fields = fields;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

}

