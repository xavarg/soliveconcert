package com.euporia.soliveconcert.models.opendata;

import com.euporia.soliveconcert.models.Current.Facet_groups;

import java.util.List;

public class Events {

    private int nhits;
    private List<Records> records;
    private List<Facet_groups> facet_groups;

    public int getNhits() {
        return nhits;
    }

    public void setNhits(int nhits) {
        this.nhits = nhits;
    }

    public List<Records> getRecords() {
        return records;
    }

    public void setRecords(List<Records> records) {
        this.records = records;
    }

    public List<Facet_groups> getFacet_groups() {
        return facet_groups;
    }

    public void setFacet_groups(List<Facet_groups> facet_groups) {
        this.facet_groups = facet_groups;
    }

    @Override
    public String toString() {
        return "Events{" +
                "nhits=" + nhits +
                ", records=" + records +
                ", facet_groups=" + facet_groups +
                '}';
    }
}
