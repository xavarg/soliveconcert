package com.euporia.soliveconcert.models.Current;

import java.util.List;

public class Facet_groups {
    private String name;
    private List<Facets> facets;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Facets> getFacetsList() {
        return facets;
    }

    public void setFacetsList(List<Facets> facetsList) {
        this.facets = facetsList;
    }

    @Override
    public String toString() {
        return "Facet_groups{" +
                "name='" + name + '\'' +
                ", facetsList=" + facets +
                '}';
    }
}
