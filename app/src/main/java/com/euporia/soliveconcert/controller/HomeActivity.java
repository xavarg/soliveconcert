package com.euporia.soliveconcert.controller;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.euporia.soliveconcert.R;
import com.euporia.soliveconcert.RecordsAdapter;
import com.euporia.soliveconcert.models.opendata.Events;
import com.euporia.soliveconcert.ui.Constant;
import com.euporia.soliveconcert.ui.FastDialog;
import com.euporia.soliveconcert.ui.Network;
import com.google.gson.Gson;

public class HomeActivity extends AppCompatActivity {
    public ListView listViewRecords;
    private Button buttonValidate;







    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        listViewRecords = (ListView) findViewById(R.id.listViewRecords);
        buttonValidate = (Button) findViewById(R.id.buttonValidate);

       buttonValidate.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Log.e("Login user","Lancer page Login");

               Intent intentLogin = new Intent(HomeActivity.this, LoginActivity.class);
                intentLogin.putExtra("isLogin",false);
               startActivity(intentLogin);

           }
       });

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(Network.isNetworkAvailable(HomeActivity.this)) {
            getVolley();
        } else {
            FastDialog.showDialog(HomeActivity.this, FastDialog.SIMPLE_DIALOG, getString(R.string.network));
        }
    }

    private void getVolley() {
        // requête HTTP avec Volley
        RequestQueue queue = Volley.newRequestQueue(HomeActivity.this);

        String url = Constant.URL_OPENDATA;

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String json) {

                        Log.e("json", "resultat: " + json);

                        Gson myGson = new Gson();
                        Events myEvent = myGson.fromJson(json, Events.class);


                        Log.e("json", "resultat: " + myEvent.getNhits());

                        Log.e("json", "resultat: " + myEvent.getFacet_groups().get(0).getName());

                        Log.e("json", "resultat: " + myEvent.getRecords().get(0).getFields().getCity());

                        listViewRecords.setAdapter(new RecordsAdapter(HomeActivity.this, R.layout.item_records, myEvent.getRecords()));
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                String json = new String(error.networkResponse.data);

            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    public void submit(View view) {
//        buttonValidate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.e("Login user","Lancer page Login");
//                Intent intentLogin = new Intent(HomeActivity.this, LoginActivity.class);
//                startActivity(intentLogin);
//
//            }
//        });
    }
}
